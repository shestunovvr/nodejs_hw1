const fs = require("fs");
const path = require("path");
const getExtention = require("../helpers/getExtention");
const bcrypt = require("bcryptjs");

let passwordsAsJSON;
let passwords;

const passwordsPath = path.join(__dirname, "..", "passwords.json");
try {
  if (!fs.existsSync(passwordsPath)) {
    fs.appendFileSync(passwordsPath, "{}", "utf8");
  }
} catch (error) {
  console.log(error);
}

try {
  passwordsAsJSON = fs.readFileSync(passwordsPath);
  passwords = JSON.parse(passwordsAsJSON) || {};
} catch (error) {
  console.log(error);
}

const fileDir = path.join(__dirname, "..", "api", "files");
const makeFilePath = (filename) => {
  try {
    return path.join(fileDir, filename);
  } catch (error) {
    console.log(error);
  }
};

const isValidPassword = async (passwords, password, filename) => {
  return !(
    passwords[filename] &&
    (password === undefined ||
      !(await bcrypt.compare(String(password), passwords[filename])))
  );
};

const alowedExtensions = ["log", "txt", "json", "yaml", "xml", "js"];

const createFile = (req, res) => {
  const { filename, content, password } = req.body;
  if (!filename) {
    res.status(400).json({
      message: "Please specify 'filename' parameter",
    });
    return;
  }
  if (!content) {
    res.status(400).json({
      message: "Please specify 'content' parameter",
    });
    return;
  }
  if (!alowedExtensions.includes(getExtention(filename))) {
    res.status(400).json({
      message: `File extention should be one of the following: ${alowedExtensions.join()}`,
    });
    return;
  }

  const filePath = makeFilePath(filename);
  fs.access(filePath, (err) => {
    if (!err) {
      res.status(400).json({
        message: `file '${filename}' already exists`,
      });
      return;
    }

    fs.writeFile(filePath, content, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          message: `Server error`,
        });
        return;
      }
      if (password && passwords) {
        let newPassword;
        try {
          newPassword = bcrypt.hashSync(password.toString(), 12);
        } catch (error) {
          console.log(error);
          res.status(500).json({
            message: "Internal server error",
          });
          return;
        }

        passwords[filename] = newPassword;
        fs.writeFile(passwordsPath, JSON.stringify(passwords), (err) => {
          if (err) {
            console.log(err);
          }
        });
      }

      res.status(200).json({
        message: "File created successfully",
      });
    });
  });
};

const updateFile = async (req, res) => {
  const { content } = req.body;
  const { password } = req.query;
  const { filename } = req.params;
  if (!filename) {
    res.status(400).json({
      message: "filename is not specified",
    });
    return;
  }
  if (!content) {
    res.status(400).json({
      message: "content is not specified",
    });
    return;
  }
  if (!alowedExtensions.includes(getExtention(filename))) {
    res.status(400).json({
      message: `File extention should be one of the following: ${alowedExtensions.join()}`,
    });
    return;
  }
  try {
    const validPassword = await isValidPassword(passwords, password, filename);
    if (!validPassword) {
      res.status(400).json({
        message: `Wrong password for file ${filename}`,
      });
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal server error",
    });
    return;
  }

  const filePath = makeFilePath(filename);
  fs.access(filePath, (err) => {
    if (err) {
      // console.log(`file "${filename}" does not exists`);
      res.status(400).json({
        message: `file ${filename} does not exists`,
      });
      return;
    }

    fs.writeFile(filePath, content, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          message: "Internal server error",
        });
        return;
      } else {
        res.status(200).json({
          message: "File edited successfully",
        });
      }
    });
  });
};

const getFiles = (req, res) => {
  fs.readdir(fileDir, (err, files) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        message: "Internal server error",
      });
      return;
    }
    res.status(200).json({
      message: "Success",
      files,
    });
  });
};

const getFile = async (req, res) => {
  const { password } = req.query;
  const { filename } = req.params;
  const filePath = makeFilePath(filename);

  try {
    const validPassword = await isValidPassword(passwords, password, filename);
    if (!validPassword) {
      res.status(400).json({
        message: `Wrong password for file ${filename}`,
      });
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal server error",
    });
    return;
  }

  fs.stat(filePath, (err, stats) => {
    if (err) {
      res.status(400).json({
        message: `No file with '${filename}' filename found`,
      });
      return;
    }
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        res.status(500).json({
          message: "Internal server error",
        });
        return;
      }
      res.status(200).json({
        message: "Success",
        filename,
        content: data,
        extension: getExtention(filename),
        uploadedDate: stats.birthtime,
      });
    });
  });
};

const deleteFile = async (req, res) => {
  const {filename} = req.params;
  const { password } = req.query;
  const filePath = makeFilePath(filename);
  try {
    const validPassword = await isValidPassword(passwords, password, filename);
    if (!validPassword) {
      res.status(400).json({
        message: `Wrong password for file ${filename}`,
      });
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal server error",
    });
    return;
  }

  fs.unlink(filePath, (err) => {
    if (err) {
      res.status(400).json({
        message: `No file with '${filename}' filename found`,
      });
      return;
    }

    if (passwords[filename]) {
      try {
        delete passwords[filename];
        fs.writeFile(passwordsPath, JSON.stringify(passwords), (err) => {
          if (err) {
            console.log(err);
          }
        });
      } catch (error) {
        console.log("failed to delete password");
        console.log(error);
        res.status(500).json({
          message: "Internal server error",
        });
        return;
      }
    }
    res.status(200).json({
      message: "File deleted successfully",
    });
  });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  updateFile
};
